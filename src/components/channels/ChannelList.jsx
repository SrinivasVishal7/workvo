import React, { Component } from 'react';
import Channel from './Channel';
import PropTypes from 'prop-types';

class ChannelList extends Component{
    render(){
        const {channels} = this.props;
        return(
            <ul>{
                channels.map(
                    chan => <Channel channel={chan} key = {chan.id} setChannel={this.props.setChannel} activeChannel={this.props.activeChannel}/>
                )
            }</ul>
        )
    }
}
ChannelList.propTypes = {
    channels: PropTypes.array.isRequired,
    setChannel : PropTypes.func.isRequired,
    activeChannel : PropTypes.object.isRequired
};

export default ChannelList;
