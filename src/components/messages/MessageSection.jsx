import React, {Component} from 'react';
import PropTypes from 'prop-types';
import MessageList from "./MessageList";
import MessageForm from "./MessageForm";

class MessageSection extends Component {
    render(){
        let {activeChannel} = this.props;
        let channelSelected;
        if (activeChannel.name === undefined) {
            channelSelected = 'Select a Channel..'
        }
        if(activeChannel.name !== undefined){
            channelSelected = activeChannel.name
        }
        return(
            <div className='messages-container panel panel-default'>
                <div className='panel-heading'>
                    <strong>{channelSelected}</strong>
                </div>
                    <div className='panel-body messages'>
                        <MessageList messages={this.props.messages}/>
                        <MessageForm activeChannel={this.props.activeChannel} addMessage={this.props.addMessage}/>
                    </div>
                </div>
        )
    }
}
MessageSection.propTypes = {
    messages : PropTypes.array.isRequired,
    activeChannel : PropTypes.object.isRequired,
    addMessage : PropTypes.func.isRequired
};
export default MessageSection;