import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Message from "./Message";

class MessageList extends Component {
    render(){
        let {messages} = this.props;
        return(
            <ul>{
                messages.map(message => <Message message={message} key={message.id} />)
            }
            </ul>
        )
    }
}

MessageList.propTypes = {
    messages : PropTypes.array.isRequired
};

export default MessageList;